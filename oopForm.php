<?php

class Form { 
	public $method = "GET";
	public $action = "oopForm.php";
	private $flag = false;

	private function createform(){
		echo "<form action=\"" . $this->action . "\" method=\"" . $this->method . "\">";
		$this->flag = true;
	}

	public function input($atributes) {
		if(!$this->flag) $this->createform();
		echo "<input ";
		foreach ($atributes as $atribute => $value) {
			echo $atribute . "=\"" . $value . "\" ";
		}
		echo "></input><br><br>";
	}

	public function input_pass($atributes) {
		if(!$this->flag) $this->createform();
		echo "<input ";
		foreach ($atributes as $atribute => $value) {
			echo $atribute . "=\"" . $value . "\" ";
		}
		echo "></input><br><br>";
	}

	public function textarea($atributes) {
		if(!$this->flag) $this->createform();
		echo "<textarea ";
		foreach ($atributes as $atribute => $value) {
			echo $atribute . "=\"" . $value . "\" ";
		}
		echo "></textarea><br><br>";
	}

	public function input_submit($atributes) {
		if(!$this->flag) $this->createform();
		echo "<input ";
		foreach ($atributes as $atribute => $value) {
			echo $atribute . "=\"" . $value . "\" ";
		}
		echo "></input><br><br>";
	}
	public function __destruct(){
		echo "</form>";
	}
}

$input = new Form;
$input->method = "POST";
$input->action = "oopForm.php";
$input->input(["placeholder"=>"login"]);
$input->input_pass(["type"=>"password","placeholder"=>"password"]);
$input->textarea(["cols"=>"21","rows"=>"5","placeholder"=>"Your text.."]);
$input->input_submit(["type"=>"submit"]);

?>