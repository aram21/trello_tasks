<?php

class Color {
	public $red;
	public $green;
	public $blue;

	public function __construct($r,$g,$b){
		$this->red = $r;
		$this->green = $g;
		$this->blue = $b;
	}
}

abstract class Component {
	public $Color;
	public $width;
	public $height;

    abstract function render();
}

class Rectangle extends Component {

	public function __construct($color,$w,$h){
		$this->Color = "rgb(".$color->red.", ".$color->green.", ".$color->blue.")";
		$this->width = $w;
		$this->height = $h;
	}

	public function render() {

		return "<div style='background-color:".$this->Color."; width:".$this->width."; height:".$this->height.";'></div>";

	}
}

class BorderedRectangle extends Rectangle {
	public $border;

	public function __construct($color,$w,$h,$border){
		parent:: __construct($color,$w,$h);
		$this->border = $border;
	}
}

class PositionedRectangle extends BorderedRectangle {
	public $top;
	public $left;

	public function __construct($color,$w,$h,$border,$top,$left){
		parent:: __construct($color,$w,$h,$border);
		$this->top = $top;
		$this->left = $left;
	}

	public function render() {
		echo "<div style='background-color:".$this->Color."; width:".$this->width."; height:".$this->height."; border:3px solid ".$this->border."; margin-top:".$this->top."px; margin-left:".$this->left."px;'></div>";
	}
}

class Renderer extends PositionedRectangle {
	public function __construct($comp) {
		echo "<br>Distance from top - ".$comp->top ."px<br>Distance from left - ". $comp->left ."px<br>Border property - 3px, solid, color = " . $comp->border . "<br>Background-color - " .$comp->Color. "<br>Width - " .$comp->width."px<br>Height - " .$comp->height."px";
	}
}

$color = new Color(127,0,0);// for rectangle's color 
$rec = new Rectangle($color, 150, 50);//for rectangle's property(width,height)
$border = new BorderedRectangle($color, 150, 50, "black");//  for rectangle's border
$pos = new PositionedRectangle($color, 150, 50, "black", 50, 150);// add rectangle's position
$rec->render();
$pos->render();// create rectangle

$rend = new Renderer($pos);// about property's
